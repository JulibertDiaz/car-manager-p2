package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response; 

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;


@Path("/cars")

public class CarManager {
	private final HashTableOA<Long, Car> cList = CarTable.getInstance();                    

	@GET
	// @Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {//returns the full list of cars
		Car[] c = new Car[cList.size()];
		for (int i =0; i<cList.getValues().size(); i++) {
			c[i] = cList.getValues().get(i);}
		return c;
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)//returns the car with the inputed ID
	public Car getCar(@PathParam("id") long id){
		if(cList.get(id)==null) {throw new WebApplicationException(404);}
		return cList.get(id);
	} 

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)//adds a car
	public Response addCar(Car car){
		if(cList.contains(car.getCarId())) {
			return Response.status(Response.Status.CONFLICT).build();
		}
		cList.put(car.getCarId(), car);
		return Response.status(201).build();
	}

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){//updates a car
		if(cList.contains(car.getCarId())) {
			cList.put(car.getCarId(), car);
			return Response.status(Response.Status.OK).build(); 
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DELETE
	@Path("{id}/delete")
	public Response deleteCar(@PathParam("id") long id){//deletes a car
		if(cList.contains(id)) {
			cList.remove(id);
			return Response.status(Response.Status.OK).build(); 
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}  

}

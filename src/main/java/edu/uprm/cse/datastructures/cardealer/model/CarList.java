package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	private static CircularSortedDoublyLinkedList<Car> cList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());

	public static CircularSortedDoublyLinkedList<Car> getInstance(){//create an instance of the list
		return cList;
	}

	public static void resetCars() {
		cList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());	
	}
}         


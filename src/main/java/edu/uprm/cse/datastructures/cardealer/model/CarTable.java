package edu.uprm.cse.datastructures.cardealer.model;
import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {
	private static HashTableOA<Long, Car>cList = new HashTableOA<Long, Car>(10,new CarComparator(), new KeyComparator());

	public static HashTableOA<Long, Car> getInstance(){//create an instance of the list
		return cList;
	}

	public static void resetCars() {
		cList = new HashTableOA<Long, Car>(10,new CarComparator(), new KeyComparator());	
	}
}         


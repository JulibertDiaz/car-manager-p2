package edu.uprm.cse.datastructures.cardealer.util;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.KeyComparator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.deser.std.DelegatingDeserializer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
public class HashTableOA<K,V> implements Map<K, V>{
	private int currentSize;
	private Object[] dealer;
	private Comparator<V> carComp;
	private Comparator<K> keyComp;
	private static final int DEFAULT= 10;

	public HashTableOA(int initialCapacity, Comparator<V> cComp, Comparator<K> kComp) {
		currentSize = 0;
		dealer = new Object[initialCapacity];
		for (int i = 0; i < dealer.length; i++) {
			dealer[i] = new MapEntry(null, null);
		}
		carComp = cComp;
		keyComp = kComp;
	}
	public HashTableOA(Comparator<V> cComp, Comparator<K> kComp) {
		this(DEFAULT, cComp, kComp);
	}

	public static class MapEntry<K,V> {
		private K key;
		private V value;
		public K getKey() {
			return key;
		}
		public void setKey(K key) {
			this.key = key;
		}
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}
		public MapEntry(K key, V value) {
			super();
			this.key = key;
			this.value = value;
		}
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize == 0;
	}

	public int hashFunction1(K key) {
		/*Gives and index based on the dealer size*/
		return Math.abs(key.hashCode() % this.dealer.length);
	}
	public int hashFunction2(K key) {
		/*Gives and index based on the current size minus the key hashcode*/
		//gives unique output for each key
		//return (this.currentSize - key.hashCode()) % this.currentSize;
		return Math.abs((3 - (key.hashCode()%3)));
	}
	public int linearProb(int index) {
		/*Checks one by one for empty spaces*/
		int c = 1;       
		int result = (index + c) % dealer.length;
		for (int i = 0; i < dealer.length; i++) {
			MapEntry<K, V> entry = (MapEntry<K, V>) dealer[result];
			if(entry.getKey()!=null) {
				result = (index + c) % dealer.length;
				c = c + 1;
			}
			else {
				return result;
			}
		}
		return result;
	}
	public int linearProb2(int index, K key) {
		/*Checks one by one for the key*/
		int c = 1;       
		int result = (index + c) % dealer.length;
		for (int i = 0; i < dealer.length; i++) {
			MapEntry<K, V> entry = (MapEntry<K, V>) dealer[result];
			if(keyComp.compare(entry.getKey(), key) != 0) {
				result = (index + c) % dealer.length;
				c = c + 1;
			}
			else {
				return result;
			}
		}
		return result;
	}
	private void reallocate() {
		/*Expands the list, keeps the order and spaces of the OG list*/
		Object[] oldDealer = new Object[dealer.length];
		Object[] newDealer = new Object[dealer.length*2];
		oldDealer = dealer;
		dealer = newDealer;
		for (int i = 0; i < oldDealer.length; i++) {
			MapEntry<K, V> entry = (MapEntry<K, V>) oldDealer[i];
			this.put(entry.getKey(), entry.getValue());
		}
	}


	@Override
	public V get(K key) {//To be tested
		/*Finds a value with a matching key,
		 *return null otherwise*/
		if(isEmpty()) {return null;}
		if(key == null) {return null;}

		MapEntry<K, V> entry = (MapEntry<K, V>) dealer[hashFunction1(key)];
		if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
			return entry.getValue();
		}

		entry = (MapEntry<K, V>) dealer[hashFunction2(key)];
		if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
			return entry.getValue();
		}

		entry = (MapEntry<K, V>) dealer[linearProb(hashFunction2(key))];
		if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
			return entry.getValue();
		}

		return null;

	}

	@Override
	public V put(K key, V value) {//to be tested
		/*if the entry exists within the list,
		 * the value is replaced
		 * else, the entry is added in the first available space found*/
		if(key == null || value == null) {return null;}
		if(this.currentSize-1 == dealer.length) {reallocate();}

		MapEntry<K, V> newEntry = new MapEntry<>(key, value);
		V oldVal = null;
		MapEntry<K, V> entry;

		//checks if it's empty
		if(isEmpty()) {
			dealer[hashFunction1(key)] = newEntry;
			currentSize++;
			return null;
		}

		//checks if it exists
		if(get(key)!=null) {
			entry = (MapEntry<K, V>) dealer[linearProb(hashFunction2(key))];
			if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
				oldVal = entry.getValue();
				dealer[linearProb(hashFunction2(key))] = newEntry;
				return oldVal;
			}
		}

		//first hash
		entry = (MapEntry<K, V>) dealer[hashFunction1(key)];
		if(entry.getKey()==null) {
			dealer[hashFunction1(key)] = newEntry;
			currentSize++;
			return oldVal;
		}
		else if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
			oldVal = entry.getValue();
			dealer[hashFunction1(key)] = newEntry;
			return oldVal;
		}

		//second hash
		entry = (MapEntry<K, V>) dealer[hashFunction2(key)];
		if(entry.getKey()==null) {
			dealer[hashFunction2(key)] = newEntry;
			currentSize++;
			return oldVal;
		}
		else if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
			oldVal = entry.getValue();
			dealer[hashFunction2(key)] = newEntry;
			return oldVal;
		}

		//linearProb
		entry = (MapEntry<K, V>) dealer[linearProb(hashFunction2(key))];
		if(entry.getKey()==null) {
			dealer[linearProb(hashFunction2(key))] = newEntry;
			currentSize++;
			return oldVal;
		}
		else if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
			oldVal = entry.getValue();
			dealer[linearProb(hashFunction2(key))] = newEntry;
			return oldVal;
		}
		return oldVal;
	}

	@Override
	public V remove(K key) {//to be tested
		if(isEmpty()) {return null;}
		if(key == null) {return null;}
		if(get(key) == null) {return null;}

		MapEntry<K, V> reset = new MapEntry<K,V>(null, null);
		MapEntry<K, V> entry;
		V oldVal;

		//first hash
		entry = (MapEntry<K, V>) dealer[hashFunction1(key)];
		if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
			oldVal = entry.getValue();
			dealer[hashFunction1(key)] = reset;
			currentSize--;
			return oldVal;
		}

		//second hash
		entry = (MapEntry<K, V>) dealer[hashFunction2(key)];
		if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
			oldVal = entry.getValue();
			dealer[hashFunction2(key)] = reset;
			currentSize--;
			return oldVal;
		}
		//linear prob 1
		entry = (MapEntry<K, V>) dealer[linearProb2(hashFunction2(key), key)];
		if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
			oldVal = entry.getValue();
			dealer[hashFunction2(key)] = reset;
			currentSize--;
			return oldVal;
		}

		//linear prob 2
		entry = (MapEntry<K, V>) dealer[linearProb2(hashFunction2(key), key)];
		if((entry.getKey()!=null) && (keyComp.compare(entry.getKey(), key) == 0)) {
			oldVal = entry.getValue();
			dealer[linearProb2(hashFunction2(key), key)] = new MapEntry<K, V>(null, null);
			currentSize--;
			return oldVal;
		}

		return null;
	}


	@Override
	public boolean contains(K key) {
		/*checks if a entry's key within the list macthes*/
		return get(key) != null;
	}

	@Override
	public SortedList<K> getKeys() {//to be tested
		SortedList<K> allKeys = new CircularSortedDoublyLinkedList<K>(keyComp);
		if (isEmpty()) {return allKeys;}
		else {
			for (int i = 0; i < dealer.length; i++) {
				MapEntry<K, V> z = (MapEntry<K, V>) dealer[i];
				if(z.getKey() != null) {
					allKeys.add(z.getKey());
				}
			}
		}
		return allKeys;
	}
	@Override
	public SortedList<V> getValues() {//to be tested
		SortedList<V> allValues = new CircularSortedDoublyLinkedList<V>(carComp);
		if (isEmpty()) {return allValues;}
		else {
			for (int i = 0; i < dealer.length; i++) {
				MapEntry<K, V> z = (MapEntry<K, V>) dealer[i];
				if(z.getValue() != null) {
					allValues.add(z.getValue());
				}
			}
		}
		return allValues;
	}

}

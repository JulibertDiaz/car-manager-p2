package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{
	

	@Override
	public int compare(Car car1, Car car2) {//Custom comparator
		if(car1 == null) {//Verifies the value of car1
			return -1;
		}
		if(car2 == null) {//Verifies the value of car2
			return 1;
		}
		//the brand, model & model option are each compared separately,
		//each comparation is returned if not the same
		if(car1.getCarBrand().compareToIgnoreCase(car2.getCarBrand()) == 0) {
			if(car1.getCarModel().compareToIgnoreCase(car2.getCarModel()) == 0) {
				 return car1.getCarModelOption().compareToIgnoreCase(car2.getCarModelOption());				
			}
			else {
				return car1.getCarModel().compareToIgnoreCase(car2.getCarModel());
			}
		}
		else {
			return car1.getCarBrand().compareToIgnoreCase(car2.getCarBrand());
		}
	}

}

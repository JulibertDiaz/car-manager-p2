package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;



public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private int currentSize; //Integer with current number of elements in list
	private DNode<E> headerNode; //Has no data and points to the first and last elements in the list. If the list is empty, it points to the tail node.
	private Comparator<E> carComp;

	public CircularSortedDoublyLinkedList(Comparator<E> comp){//Class Constructor
		this.headerNode = new DNode<E>(); 
		this.carComp = comp; //receives the custom comparator
		this.currentSize = 0; 
	} 

	@Override
	public boolean add(E obj) {//Completed
		/*Adds a new element to the list in the right order. 
		 *The method traverses the list, looking for the right position for obj.*/
		DNode<E> newNode = new DNode<E>(obj, null, null);
		DNode<E> current, next;        

		if (this.isEmpty()){//list is empty.
			newNode.setPrev(headerNode);
			newNode.setNext(headerNode);
			headerNode.setNext(newNode);
			headerNode.setPrev(newNode);
			currentSize++;
			return true;
		}

		//else if (headerNode.getNext().getElement().compareTo(obj) >= 0) { //Verifies if the first element is greater than obj.
		if(carComp.compare(headerNode.getNext().getElement(), obj) > 0) {
			headerNode.getNext().setPrev(newNode);
			newNode.setNext(headerNode.getNext());
			newNode.setPrev(headerNode);
			headerNode.setNext(newNode); 
			currentSize++;
			return true;
		}

		//else if (headerNode.getPrev().getElement().compareTo(obj) <= 0) { //Verifies if the last element is lesser than obj.
		else if(carComp.compare(headerNode.getPrev().getElement(), obj) < 0) {	
			newNode.setPrev(headerNode.getPrev());
			newNode.setNext(headerNode);
			headerNode.getPrev().setNext(newNode);
			headerNode.setPrev(newNode);
			currentSize++;
			return true;
		}

		else {
			current = headerNode.getNext();
			next = headerNode.getNext().getNext();
			while (next != headerNode) {
				//if (current.getElement().compareTo(obj) <= 0 && next.getElement().compareTo(obj) >= 0) {
				if((carComp.compare(current.getElement(), obj) <= 0) && (carComp.compare(next.getElement(), obj) >= 0)) {	
					current.setNext(newNode);
					newNode.setPrev(current);
					newNode.setNext(next);
					next.setPrev(newNode);
					currentSize++;
					return true;
				}
				else{
					current = next;
					next = next.getNext();
				}
			}
		}
		return false;
	}

	@Override
	public int size() {//Completed 
		/*Returns the number of elements in the list.*/
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {//Completed
		/*Removes the first occurrence of obj from the list. 
		 *Returns true if erased, or false otherwise.*/
		if(isEmpty() || !contains(obj)) return false;//checks if the list is empty and if the object exists within the list

		//if (headerNode.getNext().getElement().compareTo(obj) == 0){//if it's the first
		if (carComp.compare(headerNode.getNext().getElement(), obj) == 0){
			return removeFirst();
		}

		//if (headerNode.getPrev().getElement().compareTo(obj) == 0){
		if (carComp.compare(headerNode.getPrev().getElement(), obj) == 0){	
			return removeLast();
		}

		else {
			DNode <E> t = headerNode.getNext();
			for (int i = 1; i <= currentSize; i++) {//checks the whole list
				//if(t.getElement().equals(obj)) {
				if(carComp.compare(t.getElement(), obj) == 0) {
					return removeMiddle(t);
				}
				t = t.getNext();
			}
		}
		return false;
	}

	public boolean removeFirst() {//Auxiliary Method
		//Removes the first element only.
		if (currentSize == 1){
			headerNode.getNext().setElement(null);
			headerNode.getNext().setPrev(null);
			headerNode.getNext().setNext(null);
			headerNode.setNext(null);
			headerNode.setNext(headerNode);
			headerNode.setPrev(null);
			headerNode.setPrev(headerNode);
			currentSize--;
			return true; 
		}
		DNode<E> t = headerNode.getNext().getNext();
		t.getPrev().setElement(null);
		t.getPrev().setNext(null);
		t.setPrev(headerNode);
		headerNode.getNext().setPrev(null);
		headerNode.setNext(t);
		currentSize--;
		return true;	
	}

	public boolean removeLast() {//Auxiliary Method
		//Removes the last element only.
		DNode<E> t = headerNode.getPrev().getPrev();
		headerNode.getPrev().setElement(null);
		headerNode.getPrev().setNext(null);
		headerNode.setPrev(t);
		t.getNext().setPrev(null);
		t.setNext(headerNode);
		currentSize--;
		return true;
	}

	public boolean removeMiddle(DNode<E> f) {//Auxiliary Method
		DNode<E> p = f.getPrev();
		DNode<E> n = f.getNext(); 
		f.setElement(null);
		f.setNext(null);
		f.setPrev(null);
		p.setNext(null);
		p.setNext(n);
		n.setPrev(null);
		n.setPrev(p);
		currentSize--;
		return true;
	}

	@Override
	public boolean remove(int index) {//Completed.
		/*Removes the elements at position index. 
		 *Returns true if the element is erased, 
		 *or an IndexOutBoundsException if index is illegal.*/
		if (index < 0 || index > size()-1) { //Checks if the index is valid.
			throw new IndexOutOfBoundsException("Illegal Index.");
		}
		if(isEmpty()) return false; //Checks if the list is already empty.
		if (index == 0){//if it's the first
			return removeFirst();
		}
		if (index == currentSize-1){//if it's last
			return removeLast();
		}
		DNode <E> t = headerNode.getNext().getNext();
		for (int i = 1; i <= index; i++) {//iterates through the rest of the list	
			if(i == index) {
				return removeMiddle(t);
			}
			t = t.getNext();
		}
		return false;
	}

	@Override
	public int removeAll(E obj) {//Completed
		/*removes all copies of element obj, 
		 *and returns the number of copies erased.*/
		if(isEmpty()) return 0;//checks if the list is empty;
		int count = 0;
		while(contains(obj)) {
			count ++;
			remove(obj);
		}
		return count;
	}

	@Override
	public E first() {//Completed
		/*Returns the first (smallest) element in the list, 
		 *or null if the list is empty.*/
		//		if (headerNode.getNext() != headerNode) return headerNode.getNext().getElement();
		//		else return null;
		return (this.isEmpty() ? null: headerNode.getNext().getElement());
	}

	@Override
	public E last() {//Completed
		/*Returns the last (largest) element in the list, 
		 *or null if the list is empty.*/
		//		if (headerNode.getPrev() != headerNode) return headerNode.getPrev().getElement();
		//		else return null;
		return (this.isEmpty() ? null: headerNode.getPrev().getElement());
	}

	@Override
	public E get(int index) throws IndexOutOfBoundsException{//Completed.
		/*Returns the elements at position index, 
		 *or an IndexOutBoundsException if index is illegal.*/
		if (index < 0 || index > size()-1 || this.isEmpty()) {
			throw new IndexOutOfBoundsException("Illegal Index.");
		}
		else {
			DNode<E> t = headerNode.getNext();
			int i = 0;
			while (i < index) {
				t = t.getNext();
				i++;
			}
			return t.getElement();
		}
	}

	@Override
	public void clear() {//Completed.
		/*Removes all elements in the list.*/
		if(isEmpty()) return;
		while(!isEmpty()) {
			removeFirst();
		}
	}

	@Override
	public boolean contains(E e) {//Completed.
		/*Returns true if the element e is in the list or false otherwise.*/
		DNode <E> t = headerNode.getNext();
		for (int i = 1; i <= currentSize; i++) {//checks the whole list
			if(t.getElement().equals(e)) {return true;}
			t = t.getNext();
		}
		return false;
	}

	@Override
	public boolean isEmpty() {//Completed
		/*Returns true if the list is empty, or false otherwise.*/
		return size() == 0;}


	@Override
	public Iterator<E> iterator() {//Completed.		
		/*Standard Iterator*/
		return new ForwardListIterator();
	}

	private class ForwardListIterator implements Iterator<E>{//iterator constructor
		private DNode<E> currentNode;
		public ForwardListIterator() {
			this.currentNode = headerNode.getNext();
		}
		@Override
		public boolean hasNext() {
			return this.currentNode != headerNode;
		}
		@Override
		public E next() {
			if (!this.hasNext()) throw new NoSuchElementException();

			E result = this.currentNode.getElement();
			this.currentNode = this.currentNode.getNext();
			return result;
		}
	}

	public Iterator<E> iterator(int index) {//Completed.
		/*Returns a forward iterator from position index, 
		 *or an IndexOutBoundsException if index is illegal.*/
		if (index < 0 || index > size()-1) {
			throw new IndexOutOfBoundsException("Illegal Index.");
		}
		Iterator<E> iterator = new ForwardListIterator();
		for (int i = 0; i < index && iterator.hasNext(); i++) {
			iterator.next();
		}
		return iterator;
	}

	@Override
	public int firstIndex(E e) {//Completed.
		/*Returns the index (position) of the first position of element e in the list 
		 *or -1 if the element is not present.*/
		DNode <E> t = headerNode.getNext();
		for (int i = 0; i < currentSize ; i++) {
			if(t.getElement().equals(e)) {
				return i;
			}
			t = t.getNext();
		}
		return -1;
	}
	@Override
	public int lastIndex(E e) {//Completed.
		/*returns the index (position) of the last position of element e in the list 
		 *or -1 if the element is not present.*/
		DNode <E> t = headerNode.getNext();
		int index = -1;
		for (int i = 0; i < currentSize ; i++) {
			if(t.getElement().equals(e)) {
				index = i;
			}
			t = t.getNext();
		}
		return index;
	}


//	public ReverseIterator<E> reverseIterator() {//Completed
//		/*returns a reverse iterator, 
//		 *starting from the last element in the list.*/
//		return new BackwardsListIterator();
//	}
//	private class BackwardsListIterator implements ReverseIterator<E>{//iterator constructor
//		private DNode<E> currentNode;
//		public BackwardsListIterator() {
//			this.currentNode = headerNode.getPrev();
//		}
//		public boolean hasPrevious() {
//			return this.currentNode != headerNode;
//		}
//
//		public E previous() {
//			if (!this.hasPrevious()) throw new NoSuchElementException();
//			E result = this.currentNode.getElement();
//			this.currentNode = this.currentNode.getPrev();
//			return result;
//		}
//	}
//	public ReverseIterator<E> reverseIterator(int index) {//Completed.
//		/*returns a reverse iterator, 
//		 *starting from position index in the list, 
//		 *or an IndexOutBoundsException if index is illegal.*/
//		if (index < 0 || index > size()-1) {
//			throw new IndexOutOfBoundsException("Illegal Index.");
//		}
//		SortedCircularDoublyLinkedList<E>.BackwardsListIterator iterator = new BackwardsListIterator();
//		for (int i = currentSize-1; i > index && iterator.hasPrevious(); i--) {
//			iterator.previous();
//		}
//		return iterator;
//	}

	//My Doubly linked Node Class
	public class DNode<E>{
		protected E element;
		protected DNode<E> next, prev;

		/* Constructor 1*/
		public DNode() {
			next = null;
			prev = null;
			element = null; }
		
		/*Constructor 2*/
		public DNode(E d, DNode<E> n, DNode<E> p) {
			element = d;
			next = n;
			prev = p; }

		/*Setters*/
		public void setNext(DNode<E> n) { next = n;}/* Method to set the next node*/
		public void setPrev(DNode<E> p) { prev = p;}/* Method to set the previous node*/
		public void setElement(E e) { element = e;} /* Method to set the node's element*/

		/*Getters*/
		public DNode<E> getNext() { return next;}/* Method to get the next node*/
		public DNode<E> getPrev() { return prev;}/* Method to get the previous node*/
		public E getElement() { return element;} /* Method to get the element from the node*/
	}
}

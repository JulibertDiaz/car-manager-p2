//package edu.uprm.cse.datastructures.cardealer;
//
//import java.util.ArrayList;
//
//import edu.uprm.cse.datastructures.cardealer.model.Car;
//import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
//import edu.uprm.cse.datastructures.cardealer.model.KeyComparator;
//import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
//
//public class CarTableTester {
//
//	public static void main(String[] args) {
//
//
//		HashTableOA<Long, Car> cars = new HashTableOA<Long, Car>(10,new CarComparator(), new KeyComparator());
//		
//		cars.put((long) 1, new Car(1, "Toyota", "Rav4", "LE", 16000));
//		cars.put((long)  2,new Car(2, "Toyota", "Rav4", "SE", 47000));
//		cars.put((long) 3, new Car(3, "Honda", "Civic", "DX", 40000));
//		cars.put((long)  4, new Car(4, "Honda", "Accord", "LX", 22000));
//
//
//		printList(cars);
//		System.out.println(cars.size());
//
//		cars.put((long) 5, new Car(5, "Nissan", "350z", "Track", 1));
//		System.out.println("\n added one more car");
//		printList(cars);
//		System.out.println(cars.size());
//
//		cars.put((long) 6, new Car(6, "Toyota", "Corrolla", "SE", 60000));
//		System.out.println("\n added one more car");
//		printList(cars);
//		System.out.println(cars.size());
//
//	}
//
//	private static void printList(HashTableOA<Long, Car> cars) {
//		ArrayList<Car> l = (ArrayList<Car>) cars.getValues();
//		for(int i=0; i<l.size(); i++) {
//			System.out.println(l.get(i));
//		}
//
//	}
//
//}
